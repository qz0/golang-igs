# Build stage
FROM golang:1.19rc2-alpine3.16 AS builder
WORKDIR /app
COPY . .
RUN apk add --no-cache gcc libc-dev && go build -tags musl -o main main.go 
#&& go mod tidy

# Run stage.
FROM alpine:3.16
LABEL com.centurylinklabs.watchtower.enable="true"
WORKDIR /app
COPY --from=builder /app/main .
COPY --from=builder /app/app.env .

EXPOSE 8080
CMD [ "/app/main" ]
