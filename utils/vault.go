package utils

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"encoding/json"
	"github.com/go-resty/resty/v2"
	log "github.com/sirupsen/logrus"
	"ingosstrah-custom-actions/logger"
	"os"
)

type dataForToken struct {
	RoleId   string `json:"role_id"`
	SecretId string `json:"secret_id"`
}

// vaultConnect - connect to Vault
func vaultConnect(roleIdFileName string, secretIdFileName string, secretTokenUrl string, secretEnvUrl string) (string, string, error) {
	var brokerConn string
	var redisConn string

	f1, err := os.Open(roleIdFileName)
	if err != nil {
		logger.Log.Error("Can't read roleId file", err)
	}

	wr1 := bytes.Buffer{}
	sc1 := bufio.NewScanner(f1)
	for sc1.Scan() {
		wr1.WriteString(sc1.Text())
	}

	roleID := wr1.String()
	logger.Log.Info("RoleID + Size:  ", roleID, len(roleID))

	f2, err := os.Open(secretIdFileName)
	if err != nil {
		logger.Log.Error("Can't read secretId file", err)
	}

	wr2 := bytes.Buffer{}
	sc2 := bufio.NewScanner(f2)
	for sc2.Scan() {
		wr2.WriteString(sc2.Text())
	}

	secretID := wr2.String()
	logger.Log.Info("SecretID + Size:  ", secretID, len(secretID))

	//	read file for secretID
	//secretID, err := ioutil.ReadFile(secretIdFileName)
	//if err != nil {
	//	logger.Log.Error("Can't read secretId file", err)
	//} else {
	//	logger.Log.Info("SecretID: ", string(secretID))
	//}

	//	get Token
	dataForToken := dataForToken{
		RoleId:   roleID,
		SecretId: secretID,
	}

	body, err := json.Marshal(dataForToken)
	if err != nil {
		log.Error("Get Vault Token - Error with Marshaling", err)
		return "", "", err
	} else {
		logger.Log.Warn("Marshaling is fine")
		logger.Log.Warn("Token Cred: ", string(body))
	}

	var token string
	client := resty.New()
	tc := tls.Config{
		InsecureSkipVerify: true,
	}
	client.SetTLSClientConfig(&tc)
	post, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(string(body)).
		SetResult(&token). // or SetResult(AuthSuccess{}).
		Post(secretTokenUrl)
	resp, err := post, err

	if err != nil {
		log.Error("error when try to get token: ", err)
		token = string(resp.Body())
		log.Info("Error Token body: ", string(resp.Body()))
		return "", "", err
	} else {
		token = string(resp.Body())
		log.Info("Token: ", string(resp.Body()))
	}

	var secretEnv map[string]string
	//	get Env
	resp, err = client.R().
		SetHeader("X-Vault-Token", token).
		SetResult(&secretEnv). // or SetResult(AuthSuccess{}).
		Get(secretEnvUrl)

	if err != nil {
		log.Error("error when try to get secrets")
		return "", "", err
	} else {
		token = string(resp.Body())
		log.Info("Vault envs: ", string(resp.Body()))
	}

	return brokerConn, redisConn, nil
}
