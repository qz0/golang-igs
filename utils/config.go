package utils

import (
	_ "github.com/caarlos0/env/v6"
	"github.com/spf13/viper"
	"ingosstrah-custom-actions/logger"
)

type Config struct {
	RedisConnection    string `mapstructure:"REDIS_CONNECTION"`
	BrokerConnection   string `mapstructure:"BROKER_CONNECTION"`
	KafkaBrokers       string `mapstructure:"KAFKA_BROKERS"`
	KafkaLogin         string `mapstructure:"KAFKA_LOGIN"`
	KafkaPassword      string `mapstructure:"KAFKA_PASSWORD"`
	KafkaCAFile        string `mapstructure:"KAFKA_CA_FILE"`
	KafkaCertFile      string `mapstructure:"KAFKA_CERT_FILE"`
	UseSecretStorage   bool   `mapstructure:"USE_SECRET_STORAGE"`
	ExchangeName       string `mapstructure:"EXCHANGE_NAME"`
	BannerKeyName      string `mapstructure:"BANNER_KEY_NAME"`
	BannerQueueName    string `mapstructure:"BANNER_QUEUE_NAME"`
	BannerEventName    string `mapstructure:"BANNER_EVENT_NAME"`
	BannerTopicName    string `mapstructure:"BANNER_TOPIC_NAME"`
	OfferUrl           string `mapstructure:"OFFER_URL"`
	PushUrl            string `mapstructure:"PUSH_URL"`
	LogLevel           string `mapstructure:"LOG_LEVEL"`
	Version            string `mapstructure:"VERSION"`
	RoleIdSecretFile   string `mapstructure:"SECRET_FILE_ROLE_ID"`
	SecretIdSecretFile string `mapstructure:"SECRET_FILE_SECRET_ID"`
	SecretTokenUrl     string `mapstructure:"SECRET_TOKEN_URL"`
	SecretEnvUrl       string `mapstructure:"SECRET_ENV_URL"`
}

var Cfg Config

//var RMQ types.RabbitConf

// LoadConfig reads configuration from file or environment variables.
func LoadConfig(path string) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("env")

	viper.AutomaticEnv()
	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)

	logger.Log.Error(config)

	// TEST FOR VAULT
	if config.UseSecretStorage == true {
		brokerConn, redisConn, err := vaultConnect(config.RoleIdSecretFile, config.SecretIdSecretFile, config.SecretTokenUrl, config.SecretEnvUrl)
		if err != nil {
			logger.Log.Error("Can't get brockerConn/redisConn from Vault", err)
		} else {
			config.BrokerConnection = brokerConn
			config.RedisConnection = redisConn
		}
	}

	return
}
