package http

import "ingosstrah-custom-actions/types"

// url for push
var url = "https://vkusnoitochka-trigger-gate-stage.adv.ru/push-notifications"

// массив роутинга
var GeneralRoutes = []types.Route{

	//Route{
	//	"getBuildings",
	//	"GEt buildings",
	//	"GET",
	//	"/buildings",
	//	getBuildings(),
	//},

	//	***********************************************************************************
	// 	TOKEN
	//Route{
	//	"createToken",
	//	"Получаем токин по логину с паролем",
	//	"POST",
	//	"/authenticate",
	//	createToken,
	//},
	// 	/////////////////////////////////////////////
	// 	AUTH
	//
	//Route{
	//	"getProfile",
	//	"Получаем профиль пользователя",
	//	"GET",
	//	"/profile",
	//	validateMiddleware(getProfile),
	//},
	// 	***********************************************************
	// 	STATUS OK
	//	---------------------------------
	//	аутентификация
	//	---------------------------------
	types.Route{
		"statusOK",
		"Получаем инфу",
		"OPTIONS",
		"/authenticate",
		statusOK,
	},
	//	---------------------------------
	//	профиль
	//	---------------------------------
	types.Route{
		"statusOK",
		"Получаем инфу",
		"OPTIONS",
		"/profile",
		statusOK,
	},
}

var ProduceRoutes = []types.Route{

	//types.Route{
	//	"getProduce",
	//	"GEt produce",
	//	"GET",
	//	"/produce",
	//	getProduce,
	//},

	//types.Route{
	//	"consumeProduce",
	//	"GEt consume",
	//	"GET",
	//	"/consume",
	//	consumeProduce,
	//},
}

// Routes for testing
var TestRoutes = []types.Route{

	//	AWARD
	//types.Route{
	//	"testAward",
	//	"TestAward",
	//	"POST",
	//	"/test_award",
	//	awardSendReq,
	//},

	types.Route{
		"statusOK",
		"For CORS",
		"OPTIONS",
		"/test_award",
		statusOK,
	},

	//types.Route{
	//	"consumeProduce",
	//	"GEt consume",
	//	"GET",
	//	"/consume",
	//	consumeProduce,
	//},
}
