package http

import (
	"fmt"
	"github.com/gorilla/mux"
	"ingosstrah-custom-actions/logger"
	"ingosstrah-custom-actions/types"
	"net/http"
)

// AddHeaders adds all needed headers
func AddHeaders(inner http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// Here we adding headers
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS")
		// http.SetCookie(w, &http.Cookie{Name: "api_key", Value: app.GetAPIKey()})
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		inner.ServeHTTP(w, r)
		//log.Println(w.Header().Get("api_key"))
	})
}

// создание роутера
func CreateRouter(r []types.Route) *mux.Router {
	// создаем экземпляр роутера
	router := mux.NewRouter().StrictSlash(true)

	logger.Log.Warn("createRouter")
	var handler http.Handler
	//	создаем роутер

	for _, route := range r {
		// 	ссылка на функцию
		handler = route.HandlerFunc

		handler = AddHeaders(handler)
		router.
			Methods([]string{route.Method}...).
			Path(route.Path).
			Name(route.Name).
			Handler(handler)
	}
	// logger.Log.Warnf("Router: %+v", router)
	return router
}

// Serve запуск веб-сервера
func CreateWebServer(listeningPort int, httpMode bool) {

	//	Передаем экземпляр БД
	//
	// InitDB(db)
	//
	//	создаем ключик

	// заполняем роуты
	Routes := append(GeneralRoutes, ProduceRoutes...)
	Routes = append(Routes, TestRoutes...)

	// создаем экземпляр роутера
	r := CreateRouter(Routes)

	// headersOk := handlers.AllowedHeaders([]string{"Content-Type", "X-Forwarded-For"})
	// originsOk := handlers.AllowedOrigins([]string{"*"})
	// methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS", "DELETE"})
	logger.Log.Warnf("Запуск сервера %s:%d", "", listeningPort)
	// err := http.ListenAndServe(fmt.Sprintf("%s:%d", "", listeningPort), handlers.CORS(headersOk, originsOk, methodsOk)(router))

	// On the default page we will simply serve our static index page.
	r.Handle("/", http.FileServer(http.Dir("./views/")))
	// We will setup our server so we can serve static assest like images, css from the /static/{file} route
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))

	// стартуем сервер
	// если передали режим http - принудительно включаем в http
	//if httpMode {
	http.ListenAndServe(fmt.Sprintf(":%d", listeningPort), r)
	//} else {
	//	// обычный старт
	//	if err := http.ListenAndServeTLS(fmt.Sprintf(":%d", listeningPort), cert_file, key_file, r); err != nil {
	//		log.Errorf("Ошибка запуска сервера в https %d %+v", listeningPort, err)
	//
	//		if err := http.ListenAndServeTLS(fmt.Sprintf(":%d", listeningPort), cert_file2, key_file2, r); err != nil {
	//			log.Errorf("Ошибка запуска сервера в https %d %+v", listeningPort, err)
	//			http.ListenAndServe(fmt.Sprintf(":%d", listeningPort), r)
	//		}
	//	}
	//}

}
