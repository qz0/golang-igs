package types

// Request to Kafka
type BannerToKafkaReq struct {
	ClientId    string   `json:"clientId"`
	UserGuid    string   `json:"userGuid"`
	ProductId   string   `json:"productId"`
	Title       string   `json:"title"`
	Description string   `json:"description"`
	Content     string   `json:"content"`
	Image       string   `json:"image"`
	Anchors     []Anchor `json:"anchors"`
	MessageId   string   `json:"messageId"`
	Created     string   `json:"created"`
}

type Anchor struct {
	Title string `json:"title"`
	Href  string `json:"href"`
	Type  string `json:"type"`
}

// From Kafka response
type BannerFromKafkaResp struct {
	//Id          string `json:"id"`
	//Title       string `json:"title"`
	//Description string `json:"description"`
	//Product     struct {
	//	Id   string `json:"id"`
	//	Name string `json:"name"`
	//} `json:"product"`
	//Image struct {
	//	Id    string `json:"id"`
	//	Title string `json:"title"`
	//	Src   string `json:"src"`
	//} `json:"image"`
	//Anchors []struct {
	//	Title   string `json:"title"`
	//	Href    string `json:"href"`
	//	Primary string `json:"primary"`
	//	Params  struct {
	//		Offer string `json:"offer"`
	//	} `json:"params"`
	//} `json:"anchors"`
	//Created string `json:"created"`
	//Expired string `json:"expired"`
}

// Request from Rabbit
type BannerFromRabbitReq struct {
	IsCustom   bool   `json:"isCustom"`
	Returnable bool   `json:"returnable"`
	AwardType  string `json:"award_type"`
}

type BannerRabbitResp struct {
	Id      string                `json:"id"`
	Event   string                `json:"event"`
	Context BannerContextToRabbit `json:"context"`
}

type BannerData struct {
	IsCustom   bool   `json:"isCustom"`
	Returnable bool   `json:"returnable"`
	AwardType  string `json:"award_type"`
}

type BannerContextToRabbit struct {
	//OfferId string `json:"offer_id"`
}

type BannerContextFromRabbit struct {
	AwardType string `json:"award_type"`
	ProfileId string `json:"profile_id"`
}

type MsgBlock struct {
	Id      string
	Message []byte
}

type AnsBlock struct {
	Id    string
	State string
}
