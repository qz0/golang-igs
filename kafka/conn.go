package kafka

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"ingosstrah-custom-actions/logger"
	"ingosstrah-custom-actions/types"
	"ingosstrah-custom-actions/utils"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	//"github.com/confluentinc/confluent-kafka-go/kafka"

	"github.com/segmentio/kafka-go"
	//"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/segmentio/kafka-go/sasl/scram"
	"strings"
)

var (
	//brokers       = flag.String("brokers", os.Getenv("KAFKA_PEERS"), "The Kafka brokers to connect to, as a comma separated list")
	//userName      = flag.String("username", "", "The SASL username")
	//passwd        = flag.String("passwd", "", "The SASL password")
	//algorithm     = flag.String("algorithm", "", "The SASL SCRAM SHA algorithm sha256 or sha512 as mechanism")
	//topic         = flag.String("topic", "default_topic", "The Kafka topic to use")
	certFile      = ""
	keyFile       = ""
	caFile        = ""
	tlsSkipVerify = false
	//useTLS        = flag.Bool("tls", false, "Use TLS to communicate with the cluster")
	//mode          = flag.String("mode", "produce", "Mode to run in: \"produce\" to produce, \"consume\" to consume")
	//logMsg        = flag.Bool("logmsg", false, "True to log consumed messages to console")

)

func ProduceAsync(ctx context.Context, c chan string) {

	mechanism, err := scram.Mechanism(scram.SHA512, utils.Cfg.KafkaLogin, utils.Cfg.KafkaPassword)
	if err != nil {
		panic(err)
	}

	sharedTransport := &kafka.Transport{
		SASL: mechanism,
	}

	w := kafka.Writer{
		Addr:      kafka.TCP(strings.Split(utils.Cfg.KafkaBrokers, ", ")...),
		Topic:     utils.Cfg.BannerTopicName,
		Balancer:  &kafka.Hash{},
		Transport: sharedTransport,
	}
	logger.Log.Info("Connect to Kafka server")

	for msg := range c {
		err := w.WriteMessages(ctx, kafka.Message{
			Key: []byte(uuid.NewString()),
			// create an arbitrary message payload for the value
			Value: []byte(msg),
		})
		if err != nil {
			panic("could not write message " + err.Error())
		}

		log.Info("Message: ", msg)

	}

	logger.Log.Warn("Close connection to kafka server")
}

func ProduceAsyncV2(ctx context.Context, cIn chan types.MsgBlock, cOut chan types.AnsBlock) {
	brokers := []string{utils.Cfg.KafkaBrokers}

	producer, err := ConnectProducer(brokers)
	if err != nil {
		return
	} else {
		logger.Log.Info("Producer created")
	}
	defer producer.Close()
	// connect to kafka server

	//mechanism, err := scram.Mechanism(scram.SHA512, utils.Cfg.KafkaLogin, utils.Cfg.KafkaPassword)
	//if err != nil {
	//	panic(err)
	//}
	//
	//sharedTransport := &kafka.Transport{
	//	SASL: mechanism,
	//}
	//
	//w := kafka.Writer{
	//	Addr:      kafka.TCP(strings.Split(utils.Cfg.KafkaBrokers, ", ")...),
	//	Topic:     utils.Cfg.BannerTopicName,
	//	Balancer:  &kafka.Hash{},
	//	Transport: sharedTransport,
	//}
	//logger.Log.Info("Connect to Kafka server")
	//
	for msg := range cIn {
		message := &sarama.ProducerMessage{
			Topic: utils.Cfg.BannerTopicName,
			Value: sarama.StringEncoder(msg.Message),
		}

		result := make(chan types.AnsBlock, 1)

		go func() {
			partition, offset, err := producer.SendMessage(message)
			if err != nil {
				logger.Log.Error("Error sending message to kafka")
				result <- types.AnsBlock{Id: msg.Id, State: "kafka_failed"}
			} else {
				logger.Log.Infof("Message is stored in topic (%s)/%s/partition(%d)/offset(%d)\n", utils.Cfg.BannerTopicName, brokers, partition, offset)
				result <- types.AnsBlock{Id: msg.Id, State: "kafka_response"}
			}
		}()

		select {
		case <-time.After(60 * time.Second):
			cOut <- types.AnsBlock{Id: msg.Id, State: "kafka_failed"}
		case result := <-result:
			cOut <- result
		}

	}

	logger.Log.Warn("Close connection to kafka server")
}

func Produce(msg string) {

	mechanism, err := scram.Mechanism(scram.SHA512, utils.Cfg.KafkaLogin, utils.Cfg.KafkaPassword)
	if err != nil {
		panic(err)
	}
	// to produce messages
	topic := utils.Cfg.BannerTopicName
	//partition := 0

	sharedTransport := &kafka.Transport{
		SASL: mechanism,
	}

	w := kafka.Writer{
		Addr:      kafka.TCP(strings.Split(utils.Cfg.KafkaBrokers, ", ")...),
		Topic:     topic,
		Balancer:  &kafka.Hash{},
		Transport: sharedTransport,
	}

	err = w.WriteMessages(context.Background(),
		kafka.Message{Value: []byte(msg)},
	)

	if err != nil {
		logger.Log.Fatal("failed to write messages:", err)
	}

	err = w.Close()
	if err != nil {
		logger.Log.Fatal("failed to close writer:", err)
	}
}

// ConnectProducer - create connection toi kafka server
func ConnectProducer(brokersUrl []string) (sarama.SyncProducer, error) {
	logger.Log.Info("ConnectProducer")
	config := sarama.NewConfig()

	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Return.Successes = true
	config.Version = sarama.V0_10_0_0
	config.ClientID = "sasl_scram_client"
	config.Net.SASL.Enable = true
	config.Net.SASL.User = utils.Cfg.KafkaLogin
	config.Net.SASL.Password = utils.Cfg.KafkaPassword
	config.Net.SASL.Handshake = true
	config.Net.SASL.SCRAMClientGeneratorFunc = func() sarama.SCRAMClient { return &XDGSCRAMClient{HashGeneratorFcn: SHA512} }
	config.Net.SASL.Mechanism = sarama.SASLTypeSCRAMSHA512

	//config.Net.SASL.Mechanism = sarama.SASLMechanism(sarama.SASLTypeSCRAMSHA512)

	config.Producer.Retry.Max = 5

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	logger.Log.Info("Kafka cert path: ", dir)

	//clientCerts := x509.NewCertPool()
	//pemData, err := ioutil.ReadFile(utils.Cfg.KafkaCertFile)
	//logger.Log.Info("Kafka client cert path: ", dir+"/"+utils.Cfg.KafkaCertFile)
	//if err != nil {
	//	logger.Log.Info("Create client X509KeyPair is failed: ", err)
	//} else {
	//	logger.Log.Info("Cert is loaded: ", string(pemData))
	//}
	//clientCerts.AppendCertsFromPEM(pemData)

	//	Cert
	//raw, err := ioutil.ReadFile(utils.Cfg.KafkaCertFile)
	//if err != nil {
	//	logger.Log.Error("Cant open cert key file", utils.Cfg.KafkaCertFile)
	//	return nil, err
	//} else {
	//	logger.Log.Info("Cert file read successful", utils.Cfg.KafkaCertFile)
	//}
	//
	//var cert tls.Certificate
	//for {
	//	block, rest := pem.Decode(raw)
	//	if block == nil {
	//		logger.Log.Error("Cant read block of cert file")
	//		break
	//	}
	//	if block.Type == "CERTIFICATE" {
	//		logger.Log.Info("CERTIFICATE block read successful")
	//		cert.Certificate = append(cert.Certificate, block.Bytes)
	//	} else {
	//		cert.PrivateKey, err = parsePrivateKey(block.Bytes)
	//		if err != nil {
	//			logger.Log.Errorf("Failure reading private key from \"%s\": %s", utils.Cfg.KafkaCertFile, err)
	//		} else {
	//			logger.Log.Info("Key block read successful")
	//		}
	//	}
	//	raw = rest
	//}
	//
	//if len(cert.Certificate) == 0 {
	//	logger.Log.Errorf("No certificate found in \"%s\"", utils.Cfg.KafkaCertFile)
	//	return nil, errors.New(fmt.Sprintf("No certificate found in \"%s\"", utils.Cfg.KafkaCertFile))
	//} else if cert.PrivateKey == nil {
	//	logger.Log.Errorf("No private key found in \"%s\"", utils.Cfg.KafkaCertFile)
	//	return nil, errors.New(fmt.Sprintf("No private key found in \"%s\"", utils.Cfg.KafkaCertFile))
	//} else {
	//	logger.Log.Info("Certificate and Key is not empty")
	//}

	//	CA
	caCerts := x509.NewCertPool()
	pemData, err := ioutil.ReadFile(utils.Cfg.KafkaCAFile)
	logger.Log.Info("Kafka ca cert path: ", dir+"/"+utils.Cfg.KafkaCAFile)
	if err != nil {
		logger.Log.Error("Couldn't load CA cert: ", err.Error())
		return nil, err
		// Handle the error
	} else {
		logger.Log.Info("CA is loaded: ", string(pemData))
	}
	caCerts.AppendCertsFromPEM(pemData)

	config.Net.TLS.Enable = true
	config.Net.TLS.Config = &tls.Config{
		InsecureSkipVerify: true,
		RootCAs:            caCerts,
		//Certificates:       []tls.Certificate{cert},
	}
	// NewSyncProducer creates a new SyncProducer using the given broker addresses and configuration.
	conn, err := sarama.NewSyncProducer(brokersUrl, config)
	if err != nil {
		logger.Log.Error("Error - Producer isn't created: ", err)
		return nil, err
	} else {
		logger.Log.Info("Connection for producer is created")
	}
	return conn, nil
}

func parsePrivateKey(der []byte) (crypto.PrivateKey, error) {
	if key, err := x509.ParsePKCS1PrivateKey(der); err == nil {
		logger.Log.Info("It is KCS1PrivateKey")
		return key, nil
	}
	if key, err := x509.ParsePKCS8PrivateKey(der); err == nil {
		switch key := key.(type) {
		case *rsa.PrivateKey, *ecdsa.PrivateKey:
			logger.Log.Info("It is PKCS8PrivateKey (rsa or ecdsa)")
			return key, nil
		default:
			return nil, fmt.Errorf("Found unknown private key type in PKCS#8 wrapping")
		}
	}
	if key, err := x509.ParseECPrivateKey(der); err == nil {
		logger.Log.Info("It is ECPrivateKey")
		return key, nil
	}
	logger.Log.Error("Failed to parse private key")
	return nil, fmt.Errorf("Failed to parse private key")
}
