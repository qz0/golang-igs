package kafka

import (
	"encoding/json"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"ingosstrah-custom-actions/logger"
	"ingosstrah-custom-actions/types"
	_ "strconv"
	"time"
)

// BannerService - сервис для отправки к KAFKA для banner
func BannerService(Id string, _data interface{}, ctx map[string]interface{}, result *int, ch chan types.MsgBlock) {
	logger.Log.Info("BannerService")

	var err error

	var clientIdCtx string
	temp, ok := ctx["clientId"]
	if ok {
		clientIdCtx = temp.(string)
	}
	var userGuidCtx string
	temp, ok = ctx["userGuid"]
	if ok {
		userGuidCtx = temp.(string)
	}
	var productIdCtx string
	temp, ok = ctx["productId"]
	if ok {
		productIdCtx = temp.(string)
	}
	var titleCtx string
	temp, ok = ctx["title"]
	if ok {
		titleCtx = temp.(string)
	}
	var descriptionCtx string
	temp, ok = ctx["description"]
	if ok {
		descriptionCtx = temp.(string)
	}
	var contentCtx string
	temp, ok = ctx["content"]
	if ok {
		contentCtx = temp.(string)
	}
	var imageCtx string
	temp, ok = ctx["image"]
	if ok {
		imageCtx = temp.(string)
	}
	var anchorsTitleCtx string
	temp, ok = ctx["anchors_title"]
	if ok {
		anchorsTitleCtx = temp.(string)
	}
	var anchorsHrefCtx string
	temp, ok = ctx["anchors_href"]
	if ok {
		anchorsHrefCtx = temp.(string)
	}
	var anchorsTypeCtx string
	temp, ok = ctx["anchors_type"]
	if ok {
		anchorsTypeCtx = temp.(string)
	}

	//	data
	var data = _data.(map[string]interface{})
	var productIdData string
	temp, ok = data["productId"]
	if ok {
		productIdData = temp.(string)
	}
	var titleData string
	temp, ok = data["title"]
	if ok {
		titleData = temp.(string)
	}
	var descriptionData string
	temp, ok = data["description"]
	if ok {
		descriptionData = temp.(string)
	}
	var contentData string
	temp, ok = data["content"]
	if ok {
		contentData = temp.(string)
	}
	var imageData string
	temp, ok = data["image"]
	if ok {
		imageData = temp.(string)
	}
	var anchorsTitleData string
	temp, ok = data["anchors_title"]
	if ok {
		anchorsTitleData = temp.(string)
	}
	var anchorsHrefData string
	temp, ok = data["anchors_href"]
	if ok {
		anchorsHrefData = temp.(string)
	}
	var anchorsTypeData string
	temp, ok = data["anchors_type"]
	if ok {
		anchorsTypeData = temp.(string)
	}

	//	test for empty data
	var productId string
	if len(productIdData) != 0 {
		productId = productIdData
	} else {
		productId = productIdCtx
	}
	var title string
	if len(titleData) != 0 {
		title = titleData
	} else {
		title = titleCtx
	}
	var description string
	if len(descriptionData) != 0 {
		description = descriptionData
	} else {
		description = descriptionCtx
	}
	var content string
	if len(contentData) != 0 {
		content = contentData
	} else {
		content = contentCtx
	}
	var image string
	if len(imageData) != 0 {
		image = imageData
	} else {
		image = imageCtx
	}
	var anchorsTitle string
	if len(anchorsTitleData) != 0 {
		anchorsTitle = anchorsTitleData
	} else {
		anchorsTitle = anchorsTitleCtx
	}
	var anchorsHref string
	if len(anchorsHrefData) != 0 {
		anchorsHref = anchorsHrefData
	} else {
		anchorsHref = anchorsHrefCtx
	}
	var anchorsType string
	if len(anchorsTypeData) != 0 {
		anchorsType = anchorsTypeData
	} else {
		anchorsType = anchorsTypeCtx
	}

	//	kafka request
	var kafkaReq types.BannerToKafkaReq
	//var kafkaResp types.BannerFromKafkaResp

	kafkaReq.MessageId = uuid.New().String()
	kafkaReq.ClientId = clientIdCtx
	kafkaReq.UserGuid = userGuidCtx
	kafkaReq.ProductId = productId
	kafkaReq.Title = title
	kafkaReq.Description = description
	kafkaReq.Content = content
	kafkaReq.Image = image
	kafkaReq.Anchors = append(kafkaReq.Anchors, types.Anchor{
		Title: anchorsTitle,
		Href:  anchorsHref,
		Type:  anchorsType,
	})
	kafkaReq.Anchors[0].Title = anchorsTitle
	kafkaReq.Anchors[0].Href = anchorsHref
	kafkaReq.Anchors[0].Href = anchorsType
	kafkaReq.Created = time.Now().Format("2006-01-02 15:04:05")

	body, err := json.Marshal(kafkaReq)
	if err != nil {
		log.Error("Error with Marshaling", err)
	} else {
		logger.Log.Warn("Marshaling is fine: ", string(body))
	}

	if ch != nil {
		//	channel mode
		logger.Log.Info("Send message to produce by channel: ", string(body))
		ch <- types.MsgBlock{Id: Id, Message: body}
	} else {
		//	function mode
		logger.Log.Warn("Send message to produce by function: ", string(body))
		Produce(string(body))
	}

	//tr := &http.Transport{
	//	TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	//}
	////	объявляем клиента для запроса
	//client := &http.Client{Transport: tr}
	//
	//body, err := json.Marshal(restReq)
	//if err != nil {
	//	log.Error("Error with Marshaling", err)
	//} else {
	//	logger.Log.Warn("Marshaling is fine")
	//}
	//
	////	дергаем сервис
	//req, err := http.NewRequest("POST", utils.Cfg.AwardUrl, bytes.NewBuffer(body))
	//req.Header.Set("Content-Type", "application/json")
	//req.Header.Set("X-API-Key", utils.Cfg.X_API_KEY)
	//
	//if err != nil {
	//	log.Error("Error with New REquest", err)
	//} else {
	//	logger.Log.Warn("Creating request is fine", bytes.NewBuffer(body))
	//}
	//
	////	отправляем запрос
	//resp, err := client.Do(req)
	//if err != nil {
	//	log.Error("error throw sending")
	//} else {
	//	log.Info("Response RAW: ", resp.Body)
	//	if err := json.NewDecoder(resp.Body).Decode(&respFromRest); err != nil {
	//		log.Error("Aware Answer from Rst with error: ", err)
	//		//http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Данные checkStatusResponse распаковались криво : %+v\"}", err), http.StatusBadRequest)
	//	} else {
	//		logger.Log.Info("Response from REST", respFromRest)
	//	}
	//}

	*result = 0
}
