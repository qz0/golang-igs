package rabbit

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"ingosstrah-custom-actions/logger"
	"ingosstrah-custom-actions/types"
	"ingosstrah-custom-actions/utils"
)

// ProduceKafka -	публикация результатов сценария award
func ProduceKafka(eventId string, eventState string) {
	logger.Log.Info("produceAward")
	var rabbitResp types.BannerRabbitResp
	rabbitResp.Event = eventState
	rabbitResp.Id = eventId
	message, err := json.Marshal(rabbitResp)
	if err != nil {
		log.Error("Error with Marshaling", err)
	} else {
		logger.Log.Warn("Marshaling is fine")
	}

	if SendMessage(utils.Cfg.ExchangeName, eventId, string(message[:])) != nil {
		log.Error("Rabbit sending Error")
	} else {
		logger.Log.Warn("Rabbit sending Success: ", string(message[:]))
	}

	logger.Log.Warn("whats all")
}

// ProduceRabbitAsync -	публикация результатов Kafka async
func ProduceRabbitAsync(chOut chan types.AnsBlock) {
	logger.Log.Info("ProduceRabbitAsync started")
	for ansBlk := range chOut {

		var rabbitResp types.BannerRabbitResp
		rabbitResp.Event = ansBlk.State
		rabbitResp.Id = ansBlk.Id
		message, err := json.Marshal(rabbitResp)
		if err != nil {
			log.Error("Error with Marshaling", err)
		} else {
			logger.Log.Warn("Marshaling is fine")
		}

		if SendMessage(utils.Cfg.ExchangeName, ansBlk.Id, string(message[:])) != nil {
			log.Error("Rabbit sending Error")
		} else {
			logger.Log.Warn("Rabbit sending Success: ", string(message[:]))
		}

		logger.Log.Warn("whats all")
	}
	logger.Log.Info("ProduceKafkaAsync closed")
}

//
//// produceOffer -	публикация результатов сценария offer
//func produceOffer(offerId int, offerName string, eventId string) {
//	logger.Log.Info("produceOffer")
//	var rabbitResp types.OfferRabbitResp
//	rabbitResp.Context.OfferId = fmt.Sprintf("%d", offerId)
//	rabbitResp.Context.OfferName = offerName
//	rabbitResp.Event = utils.Cfg.OfferEventName
//	rabbitResp.Id = eventId
//	message, err := json.Marshal(rabbitResp)
//	if err != nil {
//		log.Error("Error with Marshaling", err)
//	} else {
//		logger.Log.Warn("Marshaling is fine")
//	}
//
//	if SendMessage(utils.Cfg.ExchangeName, eventId, string(message[:])) != nil {
//		log.Error("Rabbit sending Error")
//	} else {
//		logger.Log.Warn("Rabbit sending Success, message: ", string(message[:]))
//	}
//
//	logger.Log.Warn("whats all")
//}
