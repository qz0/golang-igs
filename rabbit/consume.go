package rabbit

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"ingosstrah-custom-actions/kafka"
	"ingosstrah-custom-actions/logger"
	"ingosstrah-custom-actions/types"
	"ingosstrah-custom-actions/utils"
)

// GetMessage - получение сообщения
func GetMessage(qName string, ch chan types.MsgBlock) {
	logger.Log.Info(qName)

	messageHandleWithChan := messageHandleWithoutChan(ch)

	var rc RabbitConnection

	//	channel mode
	rc.Consume(qName, messageHandleWithChan)

	//	function mode
	//rc.Consume(qName, messageHandle)

}

func messageHandleWithoutChan(ch chan types.MsgBlock) func(message interface{}) error {

	return func(message interface{}) error {
		var data types.RequestFromRabbit
		var result int

		logger.Log.Warn("before marshal", string(message.([]byte)))
		//logger.Log.Warn("after marshal", temp)
		err := json.Unmarshal(message.([]byte), &data)
		if err != nil {
			log.Error("Error: ", err)
		}
		//
		log.Info("after marshal", data)
		log.Warn("data.Action.Data", data.Action.Data)

		//
		switch data.Action.Name {
		case utils.Cfg.BannerEventName:
			logger.Log.Info("switch -> banner")
			kafka.BannerService(data.Id, data.Action.Data, data.Context, &result, ch)
			break
		}
		return nil

	}
}

//func messageHandle(message interface{}) error {
//	logger.Log.Warn("Version: ", utils.Cfg.Version)
//	logger.Log.Warn("Config: ", utils.Cfg)
//
//	var data types.RequestFromRabbit
//	var result int
//
//	logger.Log.Warn("before marshal", string(message.([]byte)))
//	//logger.Log.Warn("after marshal", temp)
//	err := json.Unmarshal(message.([]byte), &data)
//	if err != nil {
//		log.Error("Error: ", err)
//	}
//	//
//	log.Info("after marshal", data)
//	log.Warn("data.Action.Data", data.Action.Data)
//
//	//
//	switch data.Action.Name {
//	case utils.Cfg.BannerEventName:
//		logger.Log.Info("switch -> banner")
//		kafka.BannerService(data.Id, data.Action.Data, data.Context, &result, nil)
//		//if result == -1 {
//		//	return nil
//		//}
//		//produceAward(result, data.Id)
//		break
//		//case utils.Cfg.OfferEventName:
//		//	logger.Log.Info("switch -> offers")
//		//	http.OffersService(data.Id, data.Context, &result, &resultName)
//		//	//	orderId := <-ch
//		//	if result == -1 || resultName == "" {
//		//		return nil
//		//	}
//		//	//	orderId := <-ch
//		//	produceOffer(result, resultName, data.Id)
//		//	break
//		//case utils.Cfg.PushMessageEventName, utils.Cfg.PushDeviceEventName, utils.Cfg.PushOffersEventName, utils.Cfg.PushAwardEventName:
//		//	logger.Log.Info("switch -> " + data.Action.Name)
//		//	http.PushService(data.Id, data.Action.Name, data.Action.Id, data.Context, data.Action.Data, data.FlowId, &body)
//		//	break
//	}
//
//	return nil
//}
