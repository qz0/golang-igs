package main

import (
	"context"
	log "github.com/sirupsen/logrus"
	"ingosstrah-custom-actions/kafka"
	"ingosstrah-custom-actions/logger"
	"ingosstrah-custom-actions/rabbit"
	"ingosstrah-custom-actions/redis"
	"ingosstrah-custom-actions/types"
	"ingosstrah-custom-actions/utils"
	"io/ioutil"
)

const VERSION string = "0.0.21"

func main() {

	var chIn = make(chan types.MsgBlock)
	var chOut = make(chan types.AnsBlock)

	var err error

	utils.Cfg, err = utils.LoadConfig(".")
	if err != nil {
		log.Fatal("cannot load config", err)
	}

	utils.Cfg.Version = VERSION

	logger.Log.Warn("Version: ", utils.Cfg.Version)
	logger.Log.Warn("Config: ", utils.Cfg)

	// выставляю уровень логов. Раскоментить желаемое что бы выбрать уровень с которого отображаются логи..
	if utils.Cfg.LogLevel == "DebugLevel" {
		logger.Log.SetLevel(log.DebugLevel)
		logger.Log.Warn("DebugLevel1")
	} else {
		logger.Log.SetLevel(log.InfoLevel)
		logger.Log.Out = ioutil.Discard
	}

	//logger.Log.Warn("Current config: ", utils.Cfg)

	//go kafka.ProduceAsync(context.Background(), ch)
	go kafka.ProduceAsyncV2(context.Background(), chIn, chOut)
	go rabbit.ProduceRabbitAsync(chOut)

	redis.ActionRegistration()

	rabbit.QueuesRegistration()
	//
	//kafka.InitKafkaConn()

	rabbit.GetMessage(utils.Cfg.BannerQueueName, chIn)

	// Включаем роутер
	//http.CreateWebServer(9000, true)

}
