package redis

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"ingosstrah-custom-actions/logger"
	"ingosstrah-custom-actions/utils"
	"strings"
)

// structure for actions
type Action struct {
	name   string
	config string
}

var ctx = context.Background()

// ActionRegistration - registrtion all actions in redis
func ActionRegistration() {

	// list of actions for registration
	var actions = []Action{
		{ //	AWARD
			name: utils.Cfg.BannerEventName,
			config: `{
		  "title":"Banner request",
		  "name":"` + utils.Cfg.BannerEventName + `",
		  "class":"stateless-action",
		  "inputs":1,
		  "outputs":0,
		  "data":{
			"isCustom":true,
			"returnable":false,
			"title":"",
			"description":"",
			"productId":"",
			"image":"",
			"anchors_title":"",
			"content":"",
			"anchors_type":"",
			"anchors_href":""
		  },
		  "paramsDescription":[
			{
			  "name":"title",
			  "value":"Заголовок баннера"
			},
			{
			  "name":"description",
			  "value":"Текст баннера"
			},
			{
			  "name":"productId",
			  "value":"ID продукта (может быть пустым)"
			},
			{
			  "name":"image",
			  "value":"Изображение (может быть пустым)"
			},
			{
			  "name":"anchors_title",
			  "value":"Название кнопки (если необходимо)"
			},
			{
			  "name":"content",
			  "value":"Контент (может быть пустым)"
			},
			{
			  "name":"anchors_type",
			  "value":"Тип кнопки (может быть пустым)"
			},
			{
			  "name":"anchors_href",
			  "value":"Ссылка для перехода по кнопке"
			}
		  ]
		}`,
		},
	}

	logger.Log.Warn("Redis - Connection string: ", utils.Cfg.RedisConnection)
	//var index = strings.Index(utils.Cfg.RedisConnection, "")
	var index = strings.Index(utils.Cfg.RedisConnection, "redis://")
	if index == 0 {
		utils.Cfg.RedisConnection = utils.Cfg.RedisConnection[8:]
	}

	indexName := strings.Index(utils.Cfg.RedisConnection, ":")
	indexPass := strings.Index(utils.Cfg.RedisConnection, "@")
	var userName string
	var password string
	var address string
	if index >= 0 {
		userName = utils.Cfg.RedisConnection[:indexName]
		password = utils.Cfg.RedisConnection[indexName+1 : indexPass]
		address = utils.Cfg.RedisConnection[indexPass+1:]

		logger.Log.Warn("Redis - userName: ", userName)
		logger.Log.Warn("Redis - password: ", password)
		logger.Log.Warn("Redis - address: ", address)

	} else {
		address = utils.Cfg.RedisConnection
	}
	logger.Log.Warn("Redis - address: ", address)
	logger.Log.Warn("Redis - password: ", password)

	//	connect to redis
	client := redis.NewClient(&redis.Options{
		Username: userName,
		Addr:     address,
		Password: password,
		DB:       0,
	})

	//logger.Log.Info(client.Close())

	//	all actions in range
	for _, action := range actions {
		_, err := client.HSet(ctx, utils.Cfg.BannerKeyName, action.name, action.config).Result()
		// handle the error
		if err != nil {
			fmt.Println(err)
		}
	}
}
